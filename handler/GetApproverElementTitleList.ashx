﻿<%@ WebHandler Language="C#" Class="GetApproverElementTitleList" %>

using System;
using System.Data;
using System.Web;
using System.Collections.Generic;
using System.Data.SqlClient;
using MarvalSoftware.Data;

/// <summary>
/// ApiHandler
/// </summary>
public class GetApproverElementTitleList : IHttpHandler
{

    #region Properties

    /// <summary>
    /// Gets or sets whether or not this handler is reusable.
    /// </summary>
    public bool IsReusable
    {
        get
        {
            return false;
        }
    }

    #endregion

    #region Methods

    public void ProcessRequest(HttpContext context)
    {
        List<string> approverElementTitleList = new List<string>();
        string connString = MarvalSoftware.Servers.ServerManager.AppSettings.DatabaseConnectionString;
        using (SqlConnection db = new SqlConnection(connString))
        {
            db.Open();

            // Fetch contactid
            string query = @"  select distinct ssfd.title from selfServicefielddef ssfd
                                join selfServiceFieldParam ssfp on ssfd.pageDefinitionId = ssfp.pageDefinitionId and ssfd.fieldpos = ssfp.fieldpos
                                where fieldType = 19 ";
            using (SqlCommand command = new SqlCommand(query, db))
            {

                using (SqlDataReader reader = command.ExecuteReader())
                {
                    while (reader.Read())
                    {
                        string approverElementTitle = (string)reader["title"];
                        approverElementTitleList.Add(approverElementTitle);

                    }
                    reader.Close();
                }
            }
            db.Close();
            System.Web.Script.Serialization.JavaScriptSerializer oSerializer = new System.Web.Script.Serialization.JavaScriptSerializer();
            string json = oSerializer.Serialize(approverElementTitleList);
            context.Response.Write(json);
        }
    }
    #endregion
}

    