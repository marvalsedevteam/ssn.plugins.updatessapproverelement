﻿<%@ WebHandler Language="C#" Class="UpdateApproverElement" %>

using System;
using System.Data;
using System.Web;
using System.Collections.Generic;
using System.Data.SqlClient;
using MarvalSoftware.Data;

/// <summary>
/// ApiHandler
/// </summary>
public class UpdateApproverElement : IHttpHandler
{

    #region Properties

    /// <summary>
    /// Gets or sets whether or not this handler is reusable.
    /// </summary>
    public bool IsReusable
    {
        get
        {
            return false;
        }
    }

    #endregion

    #region Methods

    public void ProcessRequest(HttpContext context)
    {
        string role = context.Request.QueryString["role"];
        string approverElementTitle = context.Request.QueryString["approverElementTitle"];

        if(!string.IsNullOrEmpty(role) && !string.IsNullOrEmpty(approverElementTitle))
        {
            string connString = MarvalSoftware.Servers.ServerManager.AppSettings.DatabaseConnectionString;
            using (SqlConnection db = new SqlConnection(connString))
            {
                // Get List of persons with this role and convert it to json
                List<Person> persons = new List<Person>();

                db.Open();

                string query = @"  select ci.ciid, ci.name from organisationalUnitRole ourole
                                    join ci on ci.ciid = ourole.ouCIId
                                    where ci.isLogicallyDeleted = 0 and roleId =  @roleId";

                using (SqlCommand command = new SqlCommand(query, db))
                {
                    command.Parameters.AddWithValue("@roleId", int.Parse(role));

                    using (SqlDataReader reader = command.ExecuteReader())
                    {
                        while (reader.Read())
                        {
                            Person person = new Person
                            {
                                Identifier = (int)reader["ciid"],
                                Name = (string)reader["name"],
                            };
                            persons.Add(person);

                        }
                        reader.Close();
                    }
                }

                System.Web.Script.Serialization.JavaScriptSerializer oSerializer = new System.Web.Script.Serialization.JavaScriptSerializer();
                string personsJson = oSerializer.Serialize(persons);
                

                // Update approver element with json
                string queryUpdate = @"  update selfServiceFieldParam
                                        set paramValue = @persons
                                        from selfServicefielddef ssfd
                                        join selfServiceFieldParam ssfp on ssfd.pageDefinitionId = ssfp.pageDefinitionId and ssfd.fieldpos = ssfp.fieldpos
                                        where fieldType = 19 and title = @approverElementTitle";

                using (SqlCommand command = new SqlCommand(queryUpdate, db))
                {
                    command.Parameters.AddWithValue("@approverElementTitle", approverElementTitle);
                    command.Parameters.AddWithValue("@persons", personsJson);

                    command.ExecuteNonQuery();
                }

                db.Close();

                context.Response.Write("DONE!");
            }



        }
    }
    #endregion
}

public class Person
{
    public int Identifier { get; set; }
    public string Name { get; set; }
}



    