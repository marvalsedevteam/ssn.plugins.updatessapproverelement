﻿<%@ WebHandler Language="C#" Class="GetRoleList" %>

using System;
using System.IO;
using System.Net;
using System.Text;
using System.Web;
using System.Collections.Generic;
using System.Data.SqlClient;
using MarvalSoftware.Data;
using MarvalSoftware.Data.ServiceDesk;


/// <summary>
/// ApiHandler
/// </summary>
public class GetRoleList : IHttpHandler
{

    #region Properties

    /// <summary>
    /// Gets or sets whether or not this handler is reusable.
    /// </summary>
    public bool IsReusable
    {
        get
        {
            return false;
        }
    }

    #endregion

    #region Methods

    public void ProcessRequest(HttpContext context)
    {
        // set the response content type
        context.Response.ContentType = "application/json";
        MarvalSoftware.Data.ServiceDesk.RoleBroker broker = new RoleBroker();
        System.Web.Script.Serialization.JavaScriptSerializer oSerializer = new System.Web.Script.Serialization.JavaScriptSerializer();
        string json = oSerializer.Serialize(broker.GetAllRoles());
        context.Response.Write(json);
    }
    #endregion
}
    