# SSN.Plugins.ComAround

## Information

	With this plugin, MSM Admin will be able to update all Self Service Approver Element based on its name with all persons having same role


## Installation

	Please see your MSM documentation for information on how to install plugins.

## Dependence of MSM code

## Configuration

## Layout

## Supported browser
	
	Chrome
	Firefox
	Edge
	IE

## Functions

### 1.0
	
	- All approver elements will be listed in a dropdown
	- All roles will be listed in a dropdown
	- Update selected approver element with persons having same role

## Compatible Versions

| Plugin   | MSM         |
|----------|-------------|
| 1.0	   | 14.10       |